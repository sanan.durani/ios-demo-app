//
//  ViewController.swift
//  DemoApp
//
//  Created by Sanan Durani on 07/07/2021.
//

import UIKit
import SnapKit
import GenericCellControllers
import Alamofire

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    let mainView: UIView = {
        let view = UIView()
        return view
    }()
    
    let titleView: UIView = {
        let view = UIView()
        return view
    }()
    
    let titleText: UILabel = {
        let label = UILabel()
        return label
    }()
    
    let progressView:UIProgressView = {
        let progressView = UIProgressView(progressViewStyle: .bar)
        progressView.setProgress(0.5, animated: true)
        progressView.trackTintColor = Colors.LightGrayColor
        progressView.tintColor = Colors.PrimaryColor
        progressView.clipsToBounds = true
        progressView.layer.cornerRadius = 3.0
        
        progressView.layer.sublayers![1].cornerRadius = 3.0
        progressView.subviews[1].clipsToBounds = true
        return progressView

    }()
    
    let productTableView: UITableView = {
        let table = UITableView()
        table.backgroundColor = .clear
        table.separatorStyle = .none
        table.rowHeight = 150
        return table
    }()
    
    //text properties
    var fontSize:CGFloat { return CGFloat(FontSize.DefualtSize) }
    var boldFont:UIFont { return UIFont(name: FontNames.ExpoArabicBold, size: fontSize) ?? UIFont.boldSystemFont(ofSize: fontSize) }
    var normalFont:UIFont { return UIFont(name: FontNames.ExpoArabicBook, size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)}
    
    // product array
    var productArray = ["item 1", "item 2", "item 3", "item 4"]
    private var factory = ProductCellControllerFactory()
    private var cellControllers: [TableCellController] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        setupViews()
        setTitleView()
        setProgressView()
        setProductTableView()
    }
    
    func setup() {
        view.backgroundColor = .white
        title = "Shopping bag"
    }
    
    func setupViews() {
        view.addSubview(mainView)
        mainView.addSubview(titleView)
        titleView.addSubview(titleText)
        titleView.addSubview(progressView)
        mainView.addSubview(productTableView)
        
        mainView.snp.makeConstraints { make in
            make.topMargin.left.equalTo(14)
            make.right.bottomMargin.equalTo(-14)        }
        
        titleView.snp.makeConstraints { (view) in
            view.width.equalTo(mainView.snp.width)
            view.height.equalTo(80)
            view.top.equalTo(self.view.safeAreaLayoutGuide)
        }
    }
    
    private func setTitleView() {

        let attrs1 = [NSAttributedString.Key.font : boldFont as Any, NSAttributedString.Key.foregroundColor : Colors.BlackColor]
        
        let attrs2 = [NSAttributedString.Key.font : normalFont as Any, NSAttributedString.Key.foregroundColor : Colors.BlackColor]
        
        let attrs3 = [NSAttributedString.Key.font : boldFont as Any, NSAttributedString.Key.foregroundColor : Colors.PrimaryColor]
        
        let attrs4 = [NSAttributedString.Key.font : normalFont as Any, NSAttributedString.Key.foregroundColor : Colors.BlackColor]

        let attributedString1 = NSMutableAttributedString(string:"Want Free Shipping", attributes:attrs1)
        
        let attributedString2 = NSMutableAttributedString(string:"? Add ", attributes:attrs2)

        let attributedString3 = NSMutableAttributedString(string:"SR 50.00 ", attributes:attrs3)

        let attributedString4 = NSMutableAttributedString(string:"more", attributes:attrs4)

        attributedString1.append(attributedString2)
        attributedString1.append(attributedString3)
        attributedString1.append(attributedString4)
        self.titleText.attributedText = attributedString1
    
        titleText.snp.makeConstraints { text in
            text.top.equalTo(mainView).offset(18)
            text.width.equalTo(mainView.snp.width)
        }
    }
    
    private func setProgressView() {
        progressView.snp.makeConstraints { progressBarView in
            progressBarView.topMargin.equalTo(titleText).offset(24)
            progressBarView.height.equalTo(4)
            progressBarView.left.right.equalTo(titleText)
        }
    }
    
    private func setProductTableView() {
        

        productTableView.snp.makeConstraints { table in
            table.top.equalTo(titleView.snp.bottom).offset(10)
            table.left.equalTo(0)
            table.right.bottomMargin.equalTo(0)
            
            productTableView.dataSource = self
            productTableView.delegate = self
            productTableView.isUserInteractionEnabled = true
            productTableView.allowsSelection = true
            
            factory.registerCells(on: productTableView)
            cellControllers = factory.cellControllers(from: productArray)
            productTableView.reloadData()
        }

    }
    
    func numberOfSections(in tableView: UITableView) -> Int{
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellControllers.count
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return cellControllers[indexPath.row].cellFromReusableCellHolder(tableView, forIndexPath: indexPath)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        cellControllers[indexPath.row].didSelectCell()
    }
    
}
