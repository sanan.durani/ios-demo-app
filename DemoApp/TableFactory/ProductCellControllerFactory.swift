//
//  FeedCellControllerFactory.swift
//  GenericCellControllersExample
//
//  Created by Javier Valdera on 28/09/2017.
//  Copyright © 2017 Busuu Ltd. All rights reserved.
//

import Foundation
import GenericCellControllers

class ProductCellControllerFactory {

    func registerCells(on tableView: UITableView) {
        CustomCellController.registerCell(on: tableView)
    }

    func cellControllers(from elements: [String]) -> [TableCellController] {
        return elements.map { (element) in
            return CustomCellController(item: element)
        }
    }

}
