//
//  MyCellController.swift
//  DemoApp
//
//  Created by Sanan Durani on 09/07/2021.
//

import UIKit
import Foundation
import GenericCellControllers

class CustomCellController: GenericCellController<ProductTableViewCell> {
    
    fileprivate let item: String
    var itemcount = 0
        
    init(item: String) {
        self.item = item
    }
    
    fileprivate  var cellIdentifier: String {
        return String(describing: type(of: ProductTableViewCell.self))
    }
    
    override func configureCell(_ cell: ProductTableViewCell) {
        cell.name.text = item
        cell.totalCount.text = "\(itemcount)"
        cell.cellDelagate = self
        cell.tag = indexPath!.row
    }
    
    
    override func didSelectCell() {
        
    }

}

extension CustomCellController: ProductCellDelagate {
    func increment(_ cell: ProductTableViewCell) {
        itemcount += 1
        print(itemcount)
        cell.totalCount.text = "\(itemcount)"
    }
    
    func decrement(_ cell: ProductTableViewCell) {
        itemcount = itemcount <= 0 ? 0 : itemcount - 1
        cell.totalCount.text = "\(itemcount)"
    }
}
