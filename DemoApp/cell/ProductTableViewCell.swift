//
//  ProductTableViewCell.swift
//  DemoApp
//
//  Created by Sanan Durani on 09/07/2021.
//

import UIKit
import SnapKit

class ProductTableViewCell: UITableViewCell {
    
    var cellDelagate: ProductCellDelagate?

    
    let stackView:UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.spacing = 8
        stack.alignment = .fill
        stack.distribution = .fill
        return stack
    }()
    
    let productDetailView: UIView = {
        let view = UIView()
        return view
    }()
    
    let productActionView: UIView = {
        let view = UIView()
        return view
    }()
    
    let counterView: UIView = {
        let view = UIView()
        return view
    }()
    
    let productImage:UIImageView = {
        let image = UIImageView()
        return image
    }()
    
    let productDeleteButtton:UIButton = {
        let button = UIButton()
        return button
    }()
    
    let addButtton:UIButton = {
        let button = UIButton()
        button.isUserInteractionEnabled = true
        button.setImage(UIImage(systemName: "plus"), for: .normal)
        button.tintColor = Colors.BlackColor
        return button
    }()

    let removeButtton:UIButton = {
        let button = UIButton()
        button.setImage(UIImage(systemName: "minus"), for: .normal)
        button.tintColor = Colors.BlackColor
        return button
    }()

    
    let brand = UILabel()
    let name = UILabel()
    let sarNumber = UILabel()
    let totalCount = UILabel()
    var indexPath : IndexPath?

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        contentView.isUserInteractionEnabled = true

        stackView.addArrangedSubview(productImage)
        stackView.addArrangedSubview(productDetailView)
        stackView.addArrangedSubview(productActionView)
        
        productDetailView.addSubview(brand)
        productDetailView.addSubview(name)
        productDetailView.addSubview(sarNumber)
        
        productActionView.addSubview(productDeleteButtton)
        productActionView.addSubview(counterView)
        counterView.addSubview(removeButtton)
        counterView.addSubview(totalCount)
        counterView.addSubview(addButtton)
        addSubview(stackView)
        
        brand.text = "Lime Crime"
        sarNumber.text = "SAR 100"
        
        stackView.snp.makeConstraints { stack in
            stack.edges.equalTo(contentView.snp.edges)
            stack.left.topMargin.equalTo(0)
            stack.right.bottomMargin.equalTo(0)
        }
        
        productImage.snp.makeConstraints { imageView in
            imageView.width.equalTo(100)

            productImage.contentMode = .scaleAspectFit
            productImage.image = UIImage(named: "dummy-image.png")
        }
        
        productDetailView.snp.makeConstraints { view in
        }
        
        brand.snp.makeConstraints { label in
            label.topMargin.equalTo(20)
            label.left.right.equalTo(0)
        }
        
        name.snp.makeConstraints { label in
            label.top.equalTo(brand.snp.bottom)
            label.left.equalTo(brand.snp.left)
            label.right.equalTo(brand.snp.right)
        }
        
        sarNumber.snp.makeConstraints { label in
            label.left.equalTo(brand.snp.left)
            label.right.equalTo(brand.snp.right)
            label.bottomMargin.equalTo(-20)
        }
        
        //Action View
        productActionView.snp.makeConstraints { view in
            view.width.equalTo(100)
        }
        
        productDeleteButtton.snp.makeConstraints { button in
            button.centerY.equalTo(productActionView.snp.centerY).offset(-20)
            button.right.equalTo(-16)
            
            productDeleteButtton.setImage(UIImage(systemName: "trash"), for: .normal)
            productDeleteButtton.tintColor = Colors.BlackColor
        }
        
        counterView.snp.makeConstraints { view in
            view.top.equalTo(productDeleteButtton.snp.bottom).offset(20)
            view.height.equalTo(40)
            view.left.equalTo(0)
            view.right.equalTo(-8)
            
            counterView.layer.masksToBounds = true
            counterView.layer.cornerRadius = 4
            counterView.layer.borderWidth = 2
            counterView.layer.borderColor = Colors.LightGrayColor.cgColor
        }
        
        totalCount.snp.makeConstraints { label in
            label.center.equalTo(counterView.snp.center)        }
        
        addButtton.snp.makeConstraints { button in
            button.topMargin.bottom.equalTo(0)
            button.left.equalTo(totalCount.snp.right)
            button.right.equalTo(0)
            
            addButtton.addTarget(self, action: #selector(increment), for: .touchUpInside)
        }


        removeButtton.snp.makeConstraints { button in
            button.topMargin.bottom.equalTo(0)
            button.left.equalTo(0)
            button.right.equalTo(totalCount.snp.left)
            
            removeButtton.addTarget(self, action: #selector(decrement), for: .touchUpInside)
        }
        
    }
    
    @objc func increment(_sender: UIButton) {
        self.cellDelagate?.increment(self)
    }
    
    @objc func decrement() {
        self.cellDelagate?.decrement(self)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

@objc protocol ProductCellDelagate{

    func increment(_ cell: ProductTableViewCell)
    func decrement(_ cell: ProductTableViewCell)
}
