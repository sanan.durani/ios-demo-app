//
//  Constants.swift
//  DemoApp
//
//  Created by Sanan Durani on 08/07/2021.
//

import UIKit
import Foundation

struct FontNames {
    static let ExpoArabicBook = "ExpoArabic-Book"
    static let ExpoArabicMedium = "ExpoArabic-Medium"
    static let ExpoArabicLight = "ExpoArabic-Light"
    static let ExpoArabicBold = "ExpoArabic-Bold"
}

struct FontSize {
    static let DefualtSize = 13
}

struct Colors {
    static let PrimaryColor = UIColor.init(red: 202/250, green: 40/250, blue: 232/250, alpha: 1.0)
    static let LightGrayColor = UIColor.init(red: 236/250, green: 236/250, blue: 236/250, alpha: 1.0)
    static let BlackColor = UIColor.black
}

extension UIView {
    func addTopBorder(with color: UIColor?, andWidth borderWidth: CGFloat) {
        let border = UIView()
        border.backgroundColor = color
        border.autoresizingMask = [.flexibleWidth, .flexibleBottomMargin]
        border.frame = CGRect(x: 0, y: 0, width: frame.size.width, height: borderWidth)
        addSubview(border)
    }

    func addBottomBorder(with color: UIColor?, andWidth borderWidth: CGFloat) {
        let border = UIView()
        border.backgroundColor = color
        border.autoresizingMask = [.flexibleWidth, .flexibleTopMargin]
        border.frame = CGRect(x: 0, y: frame.size.height - borderWidth, width: frame.size.width, height: borderWidth)
        addSubview(border)
    }

    func addLeftBorder(with color: UIColor?, andWidth borderWidth: CGFloat) {
        let border = UIView()
        border.backgroundColor = color
        border.frame = CGRect(x: 0, y: 0, width: borderWidth, height: frame.size.height)
        border.autoresizingMask = [.flexibleHeight, .flexibleRightMargin]
        addSubview(border)
    }

    func addRightBorder(with color: UIColor?, andWidth borderWidth: CGFloat) {
        let border = UIView()
        border.backgroundColor = color
        border.autoresizingMask = [.flexibleHeight, .flexibleLeftMargin]
        border.frame = CGRect(x: frame.size.width - borderWidth, y: 0, width: borderWidth, height: frame.size.height)
        addSubview(border)
    }
}
